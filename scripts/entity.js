export var onTurnChanged = 1,
	onCollition,
	onDamage;

export const playerUpdate = function(runtime) {
	if(this.behaviors && this.behaviors.TileMovement ){
		this.currentX = this.behaviors.TileMovement.getGridPosition()[0] //Math.floor(player.x/32)
		this.currentY = this.behaviors.TileMovement.getGridPosition()[1] //Math.floor(player.y/32)
	}
	
	if( typeof this.instVars !== 'undefined' ){
		//console.log(this.instVars)
		const vars = this.instVars
		if( vars.leftKey && runtime.keyboard.isKeyDown( vars.leftKey.charCodeAt(0)) ){
			this.move(this.currentX-1,this.currentY)
		}
		if( vars.rightKey && runtime.keyboard.isKeyDown( vars.rightKey.charCodeAt(0)) ){
			this.move(this.currentX+1,this.currentY)
		}
		if( vars.upKey && runtime.keyboard.isKeyDown( vars.upKey.charCodeAt(0)) ){
			this.move(this.currentX,this.currentY-1)
		}
		if( vars.downKey && runtime.keyboard.isKeyDown( vars.downKey.charCodeAt(0)) ){
			this.move(this.currentX,this.currentY+1)
		}
	}
}
