
// Import any other script files here, e.g.:
// import * as myModule from "./mymodule.js";

import * as entity from "./entity.js"
import TDMap from "./TDMap.js" 
import SuperMap from "./SuperMap.js"

var map = new TDMap();
var smap = new SuperMap();

const prefabWith = 8
const prefabHeight = 8

var lastX = 0
var lastY = 0

runOnStartup(async runtime =>
{
	// Code to run on the loading screen.
	// Note layouts, objects etc. are not yet available.
	
	runtime.addEventListener("beforeprojectstart", () => OnBeforeProjectStart(runtime));
});

async function OnBeforeProjectStart(runtime)
{
	// Code to run just before 'On start of layout' on
	// the first layout. Loading has finished and initial
	// instances are created and available to use here.
	Start(runtime)
	
	runtime.addEventListener("tick", () => Tick(runtime));
}

function Start(runtime){	
	let tilemap = runtime.objects.Tilemap.getFirstInstance();	
	let w = tilemap.mapWidth/prefabWith;
	let h = tilemap.mapHeight/prefabHeight;	
	/*for( let i = 0 ; i < w ; i++  ){
		for( let j = 0 ; j < h ; j++  ){
			const curtile = tilemap.getTileAt(i,j)
			const randtile = Math.floor(Math.random()*75.999) 
			if(curtile < 0){
				//tilemap.setTileAt(i,j,randtile)
			}
		}
	}*/
	
	
	for( let i = 0 ; i < w ; i++  ){
		for( let j = 0 ; j < h ; j++  ){
			let prefab = runtime.objects.Prefab01.createInstance(0,i*32*prefabWith,j*32*prefabHeight,true)
			//TODO: COMPROBAR NO HAYA UN PREFAB IDENTICO AL REDEDOR
		}
	}	
	
	let player = runtime.objects.Player.getFirstInstance();
	console.log(player)
	player.update = entity.playerUpdate
	//player.setup(runtime,"player")
	map.set(2,4,"hola")
	map.set(1,1,1)
	map.set(1,1,"hola")
	
	smap.set({id:1,name:"hola"},"id","name")
	smap.set({id:2,name:"hola"},"id",["name"])
	smap.set({id:3,name:"perro"},"id",["name"])
	smap.set({id:3,name:"gato",x:1,y:2},"id",["name",function(x,y){ return x+"_"+y }])
	smap.set({id:3,name:"gato",x:1,y:2},"id",["name",["y","x"]])
	
	console.log(smap.getData())
	
	
	//SETUP
	for( let obs in runtime.objects ){			
		for( let ob of runtime.objects[obs].getAllInstances() ){			
			ob.setup()
		}
	}
	
	for( let obs in runtime.objects ){			
		for( let ob of runtime.objects[obs].getAllInstances() ){			
			if( ob.objectType.name == "EnemigoMuerte" ){
				ob.update = entity.playerUpdate
			}
		}
	}
	//player.instVars.downKey = "S"
	//console.log(map.getData())
	console.log(smap.getData())
	
}

function Tick(runtime)
{	
	// Code to run every tick
	let player = runtime.objects.Player.getFirstInstance();
	
	
	//console.log(currentX)
	
	/*if(runtime.keyboard.isKeyDown("A".charCodeAt(0))){
		console.log(player.behaviors.TileMovement.canMoveTo(currentX+1,currentY))
		console.log(player)
		player.look()
	}*/
	
	/*if( currentX != lastX ||  currentY != lastY){
		runtime.globalVars.turn = runtime.globalVars.turn + player.instVars.speed
		for( let obs in runtime.objects ){			
			for( let ob of runtime.objects[obs].getAllInstances() ){
				//console.log(ob)
				//console.log(typeof ob.onTurnChange);
				if( ob.has('onTurnChange') ){
					ob.onTurnChange()
				}
				//ob.update(runtime)
			}
			
		}
	}*/
	
	for( let obs in runtime.objects ){			
		for( let ob of runtime.objects[obs].getAllInstances() ){
			if( ob.update ){
				ob.update(runtime)
			}
		}
	}
}

IInstance.prototype.setup = function(runtime) {
	this.obType = this.objectType.name		
	//map.set(this.x,this.y,this.uid)
	smap.set(this,"uid",["x"])
	smap.set(this,"uid",[["x","uid"],["x","y"]])
	/*console.log(this.uid)
	console.log( this.x,this.y )
	console.log( map.get(this.uid) )
	console.log("-----")*/
	
}

//IInstance.prototype.update = 

IInstance.prototype.move = function(tx,ty) {
	if(this.behaviors && this.behaviors.TileMovement && !this.behaviors.TileMovement.isMoving() ){
		if( this.behaviors.TileMovement.canMoveTo(tx,ty) ){
			console.log(this.behaviors.TileMovement)
			this.behaviors.TileMovement.setGridPosition(tx,ty)
		}
		else{			
			console.log(this.look(tx,ty))
		}		
	}
}

IInstance.prototype.look = function(tx,ty) {	
	let tilepos = this.behaviors.TileMovement.getGridPosition()	
	const dx = (tx-tilepos[0])*32
	const dy = (ty-tilepos[1])*32	
	this.behaviors.LineOfSight.castRay(this.x+16,this.y+16,this.x+16+dx,this.y+16+dy)	
	return(this.runtime.getInstanceByUid( this.behaviors.LineOfSight.ray.hitUid ))	
}







